"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var common_tasks_1 = require("./common-tasks");
exports.locatorsHome = {
    customerLoginButton: {
        type: common_tasks_1.IdentificationType[common_tasks_1.IdentificationType.Xpath],
        value: '//button[contains(text(), "Customer Login")]'
    },
    allButtons: {
        type: common_tasks_1.IdentificationType[common_tasks_1.IdentificationType.Xpath],
        value: '//button'
    }
};
var HomePage = /** @class */ (function (_super) {
    __extends(HomePage, _super);
    function HomePage() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.customerLoginButton = _this.elementLocator(exports.locatorsHome.customerLoginButton);
        _this.allButtons = _this.elementLocator(exports.locatorsHome.allButtons);
        return _this;
    }
    return HomePage;
}(common_tasks_1.CommonTasks));
exports.HomePage = HomePage;
