"use strict";
exports.__esModule = true;
var protractor_1 = require("protractor");
var IdentificationType;
(function (IdentificationType) {
    IdentificationType[IdentificationType["Id"] = 0] = "Id";
    IdentificationType[IdentificationType["Name"] = 1] = "Name";
    IdentificationType[IdentificationType["Css"] = 2] = "Css";
    IdentificationType[IdentificationType["Xpath"] = 3] = "Xpath";
    IdentificationType[IdentificationType["LinkText"] = 4] = "LinkText";
    IdentificationType[IdentificationType["PartialLinkText"] = 5] = "PartialLinkText";
    IdentificationType[IdentificationType["ClassName"] = 6] = "ClassName";
    IdentificationType[IdentificationType["ButtonText"] = 7] = "ButtonText";
    IdentificationType[IdentificationType["Model"] = 8] = "Model";
})(IdentificationType = exports.IdentificationType || (exports.IdentificationType = {}));
var CommonTasks = /** @class */ (function () {
    function CommonTasks() {
    }
    // Identify Web Elements
    CommonTasks.prototype.elementLocator = function (obj) {
        switch (obj.type) {
            case IdentificationType[IdentificationType.Xpath]:
                return protractor_1.element(protractor_1.by.xpath(obj.value));
            default:
                return protractor_1.element(protractor_1.by.xpath(obj.value));
        }
    };
    return CommonTasks;
}());
exports.CommonTasks = CommonTasks;
