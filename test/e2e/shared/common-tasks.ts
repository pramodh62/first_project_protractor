import { element, by } from 'protractor';

export enum IdentificationType {
    Id,
    Name,
    Css,
    Xpath,
    LinkText,
    PartialLinkText,
    ClassName,
    ButtonText,
    Model
}


export class CommonTasks {
    // Identify Web Elements
    elementLocator(obj: any) {
        switch (obj.type) {
            case IdentificationType[IdentificationType.Xpath]:
                return element(by.xpath(obj.value));
            default:
                return element(by.xpath(obj.value));
        }
    }
}
