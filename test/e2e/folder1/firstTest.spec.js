"use strict";
exports.__esModule = true;
var protractor_1 = require("protractor");
var home_1 = require("../shared/home");
var homePage = new home_1.HomePage();
describe('Going to write the First Test', function () {
    it('Should pass without any issues', function () {
        protractor_1.browser.get('http://www.way2automation.com/angularjs-protractor/banking/#/login/');
        homePage.customerLoginButton.click();
        homePage.allButtons.getText().then((function (text) {
            console.log('The heading is: ' + text);
        }));
    });
});
