import { browser, element, by } from 'protractor';
import { HomePage } from '../shared/home';

const homePage = new HomePage();

describe('Going to write the First Test', () => {
    it('Should pass without any issues', () => {
        browser.get('http://www.way2automation.com/angularjs-protractor/banking/#/login/');
        homePage.customerLoginButton.click();
        homePage.allButtons.getText().then((text => {
            console.log('The heading is: ' + text);
        }));

    });
});
